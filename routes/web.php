<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 /*
Route::get('/', function () {
    return view('index');
});*/

Route::get('/', 'ProduitsController@home')->name('index');

Route::get('Produit/{id}', 'ProduitsController@show')->name('Produit');
Route::get('Categorie/{id}', 'ProduitsController@Categorie')->name('Categorie');

Route::get('/profile', 'ProduitsController@Profile')->middleware('auth')->name('Profil'); 
Route::get('/Home', 'ProduitsController@indexProfil')->middleware('auth')->name('Home'); 
Route::get('/Liste_de_Votre_Livre', 'ProduitsController@Profil_Liste_De_Votre_Livre')->middleware('auth')->name('Liste_de_Votre_Livre'); 
Route::get('/Achetes_Livre', 'ProduitsController@Achetes_Livre')->middleware('auth')->name('Achetes'); 
Route::get('/Ajoute_Livre', 'ProduitsController@Ajoute_Livre')->middleware('auth')->name('Ajoute'); 
Route::get('/Liste_de_jaime_Livre', 'ProduitsController@Liste_de_jaime_Livre')->middleware('auth')->name('Liste_de_jaime_Livre'); 
Route::Post('/AjouteProduit', 'ProduitsController@AjouteProduit')->middleware('auth')->name('AjouteProduit'); 
Route::delete('/destroy/{id}', 'ProduitsController@destroy')->middleware('auth')->name('destroy'); 
Route::get('/Modification/{id}', 'ProduitsController@Modification')->middleware('auth')->name('Modification'); 
Route::POST('/Edit/{id}', 'ProduitsController@Edit')->middleware('auth')->name('Edit'); 
Route::get('/jaime/{id}', 'ProduitsController@btn_jaime')->middleware('auth')->name('jaime'); 
Route::get('/achetes/{id}', 'ProduitsController@achetes')->middleware('auth')->name('achetes'); 

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
 

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produit;
use App\Produitimage;
use App\Categorieproduit;
use App\Categorieimage;
use App\Sabonnere;
use App\Achete;
use Illuminate\Support\Facades\Auth;
use DB;

class ProduitsController extends Controller
{


    public function indexProfil()
    {
        $produit = Produit::where('id_user',  Auth::user()->id )->get(); 
        
        $Produitimage2 =DB::table('Produits') 
            ->join('produitimages', 'Produits.id',   'produitimages.id_produit')
            ->join('Categorieproduits', 'Produits.idcategorie',   'Categorieproduits.id')
            ->select( 'Categorieproduits.nom AS nomcategorie' , 'Produits.*','produitimages.*',DB::raw('count(Produits.id) as countProduits'),DB::raw('count(Categorieproduits.id) as countCategorie'))   
            ->groupBy('Produits.id')
            ->get(); 

        
        return view('\profil\index', compact('Produitimage2'));
    } 
    

  


    public function Profil_Liste_De_Votre_Livre()
    {
        $VotreProduit =DB::table('Produits')
            ->join('Categorieproduits', 'Produits.idcategorie',   'Categorieproduits.id') 
            ->select( 'Categorieproduits.nom AS nomcategorie', 'Produits.*')  
            ->where('Produits.id_user', Auth::user()->id  )
            ->get();
        
        $produitimages = produitimage::all(); 
        $Categorieproduit = Categorieproduit::all();
        return view('\profil\Liste_DE_Votre_Livre', compact('VotreProduit','produitimages','Categorieproduit'));
    }

    public function Delete(Request $request , $id)
    {  
       
    }
    public function Profile()
    {  
        return view('\profil\Profile');
    }

    public function Achetes_Livre()
    {  

        $Achetes_Livre =DB::table('achetes')
            ->join('Produits', 'achetes.id_produit', '=', 'Produits.id')
            ->join('Categorieproduits', 'Produits.idcategorie', '=', 'Categorieproduits.id') 
            ->select('Produits.*', 'Categorieproduits.nom AS nomcategorie','achetes.*')
            ->where('achetes.id_user' , Auth::user()->id)
            ->groupBy('achetes.id')
            ->get();

        return view('\profil\Achetes_Livre', compact('Achetes_Livre'));
    }
    public function Ajoute_Livre()
    {  $Categorieproduit = Categorieproduit::all(); 

        return view('\profil\Ajoute_Livre',compact('Categorieproduit'));
    }

    public function AjouteProduit()
    {   $format = 'Y-m-d H:i:s';
        $date = \DateTime::createFromFormat($format, '2009-02-15 15:16:17');
        $date =  $date->format('Y-m-d H:i:s') ;

        $nom = request('nom');
        $description = request('description');
        $prix = request('prix');
        $remise = request('remise');
        $Categorie = request('Categorie');
        $Produit= new Produit();
        $Produit->nom=$nom;
        $Produit->description=$description;
        $Produit->prix=$prix;
        $Produit->remise=$remise;
        $Produit->idcategorie=$Categorie;
        $Produit->slug= $date.Auth::user()->name;
        $Produit->id_user=Auth::user()->id;
        $Produit->save(); 

        return back();
    }
    
    public function Liste_de_jaime_Livre()
    {  
        $Liste_de_jaime_Livre =DB::table('sabonneres')
            ->join('Produits', 'sabonneres.id_produit', '=', 'Produits.id')
            ->join('Categorieproduits', 'Produits.idcategorie', '=', 'Categorieproduits.id') 
            ->select('Produits.*', 'Categorieproduits.nom AS nomcategorie','sabonneres.*')
            ->where('sabonneres.id_user' , Auth::user()->id)
            ->groupBy('sabonneres.id') 
            ->get();

        // $Liste_de_jaime_Livre =DB::table('Sabonneres') 
        //     ->select('Sabonneres.*')
        //     ->where('Sabonneres.id_user' , Auth::user()->id) 
        //     ->get();

        return view('\profil\Liste_de_jaime_Livre',compact('Liste_de_jaime_Livre'));
    }
    
    public function btn_jaime($id)
     {  
    //     $SabonnerePres =  DB::table('sabonneres')
    //                     ->where('id_produit',  $id )
    //                     ->where('id_user',  Auth::user()->id )
    //                     ->get();
    //     if(! is_null ($SabonnerePres))
    //     {
            $Sabonnere= new Sabonnere();
            $Sabonnere->id_produit=$id;
            $Sabonnere->id_user=Auth::user()->id;
            $Sabonnere->slug=Auth::user()->slug;
            $Sabonnere->save(); 
    // }
        return back();
    }
     

    public function achetes($id)
     {  
            $Achete= new Achete();
            $Achete->id_produit=$id;
            $Achete->id_user=Auth::user()->id;
            $Achete->slug=Auth::user()->slug;
            $Achete->quantite= request('qty');
            $Achete->save();  
        return back();
    }


     public function home()
    { 
        $Produit = Produit::all();  
        $Produitimage = Produitimage::all();  
        
        $Produitimage2 =DB::table('produitimages')
            ->join('Produits', 'produitimages.id_produit', '=', 'Produits.id')
            ->select('Produits.*', 'produitimages.*')
            ->groupBy('Produits.id')
            ->get();
        $Categorieimage2 =DB::table('Categorieimages')
            ->join('Categorieproduits', 'Categorieimages.idcategories', '=', 'Categorieproduits.id')
            ->join('Produits', 'Categorieproduits.id', '=', 'Produits.idcategorie') 
            ->select('Categorieproduits.*', 'Categorieimages.*',DB::raw('count(Produits.id) as count'))
            ->groupBy('Categorieproduits.id')
            ->get();

        
         
        $Categorieproduit = Categorieproduit::all(); 
        return view('index', compact('Produit','Categorieproduit','Produitimage2','Categorieimage2' ));
         
         
    }

    public function Categorie($id)
    {
        $Categorieproduit = Categorieproduit::findOrFail($id); 
        $Categorieimage = Categorieimage::where('idcategories', $Categorieproduit->id)->get();
         
        $Categorieimage2 =DB::table('Categorieimages')
            ->join('Categorieproduits', 'Categorieimages.idcategories', '=', 'Categorieproduits.id')
            ->join('Produits', 'Categorieproduits.id', '=', 'Produits.idcategorie')
            ->select('Categorieproduits.*', 'Categorieimages.*',DB::raw('count(Produits.id) as count'))
            ->groupBy('Categorieproduits.id')
            ->get();
        return view('categories-detailer', compact('Categorieproduit','Categorieimage','Categorieimage2'));

 
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Produit = Produit::findOrFail($id); 
        $Produitimage = Produitimage::where('id_produit', $Produit->id)->get();
         
        $ListeProduit =DB::table('produitimages')
            ->join('Produits', 'produitimages.id_produit', '=', 'Produits.id')
            ->select('Produits.*', 'produitimages.*')
            ->where('Produits.idcategorie', $Produit->idcategorie )
            ->groupBy('Produits.id')
            ->get();
        return view('product-details', compact('Produit','Produitimage','ListeProduit'));  
    }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {
    //     //
    // }
   public function Modification( $id)
    {
        $Categorieproduit = Categorieproduit::all(); 
        $produits = Produit::where('id', '=', $id)->first();
        return view('\profil\ModificationProduit', compact('produits','Categorieproduit'));  

    }
    /**ModificationProduit
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Edit( $id)
    { 
        $format = 'Y-m-d H:i:s';
        $date = \DateTime::createFromFormat($format, '2009-02-15 15:16:17');
        $date =  $date->format('Y-m-d H:i:s') ;

        $nom = request('nom');
        $description = request('description');
        $prix = request('prix');
        $remise = request('remise');
        $Categorie = request('Categorie');
        
        $Produit= Produit::find($id);

        $Produit->nom=$nom;
        $Produit->description=$description;
        $Produit->prix=$prix;
        $Produit->remise=$remise;
        $Produit->idcategorie=$Categorie;
        $Produit->slug= $date.Auth::user()->name;
        $Produit->id_user=Auth::user()->id;
        $Produit->save(); 

        return back();
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //  return $request->all(); 
        $Produit1 = Produit::findOrFail($id); 
        $Produit1->delete();
        return back();
    }
}

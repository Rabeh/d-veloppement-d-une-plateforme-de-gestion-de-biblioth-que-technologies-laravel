@extends('welcome')

@section('home')        
        <!--================Slider Area =================-->
        <div class="container"> 
        <div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="{{ asset('img\home-carousel\book-2855857__340.jpg')}}" alt="Los Angeles" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Los Angeles</h3>
        <p>We had such a great time in LA!</p>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="{{ asset('img\home-carousel\book-1769228__340.jpg')}}" alt="Chicago" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Chicago</h3>
        <p>Thank you, Chicago!</p>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="{{ asset('img\home-carousel\livre.jpg')}}" alt="New York" width="1100" height="500">
      <div class="carousel-caption">
        <h3>New York</h3>
        <p>We love the Big Apple!</p>
      </div>   
    </div>
  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
</div>
        <!--================End Slider Area =================-->
       
        <section class="from_blog_area">
            <div class="container"> 
            <div class="s_m_title">
                    <h2>Liste Des Categories </h2>
                </div>
                <br>
            <div class="from_blog_inner"> 
                <div class="l_product_slider owl-carousel">
                @foreach( $Categorieimage2 as $Categorieimage   ) 
                    <div class="card" style="width: 18rem;">
            <a href="{{route('Categorie',['id'=>$Categorieimage->id])}}" class="card-link">    <img class="card-img-top" width="200px"  height="250px" src="{{ Voyager::image($Categorieimage->image) }}" alt="Card image cap"> </a>
                        <div class="card-body">
                            <h5 class="card-title"> <p class="font-weight-bold"><a href="{{route('Categorie',['id'=>$Categorieimage->id])}}" class="card-link">{{$Categorieimage->nom}}</a></p></h5>
                             <p class="font-weight-bold"> {{str_limit($Categorieimage->description, $limit = 45, $end = '...') }}</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><p class="font-weight-bold">Appartient {{$Categorieimage->count}}   Livre</p></li>
                            {{-- <li class="list-group-item"><p class="font-weight-bold">Appartient ///// Livre  jaime dans ce Categorie</p></li>
                            <li class="list-group-item">//////////</li> --}}
                        </ul>
                        <div class="card-body">
                             
                            <a href="{{route('Categorie',['id'=>$Categorieimage->id])}}" class="card-link">Plus D'information</a>
                        </div>
                    </div>
                @endforeach 
                </div>
                </div>
                </div>
        </section>
            
    
        
        <!--================Our Latest Product Area =================-->
        
        <section class="our_latest_product">
            <div class="container">
                <div class="s_m_title">
                    <h2>Liste Des Livre </h2>
                     
                </div>
                <div class="l_product_slider owl-carousel">
                
                @foreach( $Produitimage2 as $Produits   ) 
                    <div class="item">
                        <div class="l_product_item">  
                            <div class="l_p_img"> 
                            <a href="{{route('Produit',['id'=>$Produits->id_produit])}}" class="card-link">    <img src="{{ Voyager::image($Produits->image) }}" width="270px"  height="320px" alt=""> </a>
                            </div> 
                            <div class="l_p_text">
                                <ul>
                                    <li class="p_icon"><a href="{{route('Produit',['id'=>$Produits->id_produit])}}"><i class="icon_piechart"></i></a></li>
                                    {{-- <li><a class="add_cart_btn" href="/achetes/{{$Produits->id}}">Add To Cart</a></li> --}}
                                    @auth
                                    <li class="p_icon"><a href="/jaime/{{$Produits->id_produit}}"><i class="icon_heart_alt"></i></a></li>
                                    @endauth
                                </ul>
                                <a href="{{route('Produit',['id'=>$Produits->id_produit])}}" class="card-link"><h4>{{$Produits->nom}}</h4></a>
                                @if($Produits->remise == null)
                                <h5>{{$Produits->prix }}$</h5>
                                @else
                                <?php  $x =$Produits->prix * (1-$Produits->remise/100) ?>
                               
                                <h5><del>{{$Produits->prix }}$</del><br> avec remise {{$Produits->remise}}% = {{$x }}$</h5>
                                @endif
                            </div>
                        </div> 
                    </div>  
                 @endforeach 
                 </div>
            </div>
        </section>
        <!--================End Our Latest Product Area =================-->
        
         
        
        <!--================Featured Product Area =================-->
        <section class="feature_product_area">
            <div class="container">
                <div class="f_p_inner">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="f_product_left">
                                <div class="s_m_title">
                                    <h2>Liste de categories avec leur Livres</h2>
                                </div>
                                 
                            </div>
                        </div>


                        <div class="col-lg-9">
                            <div class="fillter_slider_inner">
                                <ul class="portfolio_filter">
                                    <li class="active" data-filter="*"><a href="#">Tousles Produits</a></li>
                                    @foreach( $Categorieproduit as $Categorieproduits   )  
                                        <li data-filter=".{{$Categorieproduits->id}}"><a href="#">{{$Categorieproduits->nom}}</a></li>
                                    @endforeach 
                                </ul>
                                <div class="fillter_slider owl-carousel">
                                @foreach( $Produitimage2 as $Produits   ) 
                                    <div class="item {{$Produits->idcategorie}}">
                                        <div class="fillter_product_item bags">
                                            <div class="f_p_img">
                                                <a href="{{route('Produit',['id'=>$Produits->id_produit])}}" class="card-link"><img src="{{ Voyager::image($Produits->image) }}"  width="270px"  height="250px"  alt=""></a>
                                                <a href="{{route('Produit',['id'=>$Produits->id_produit])}}" class="card-link"><h5 class="sale">{{$Produits->nom}}</h5></a>
                                            </div>
                                            <div class="f_p_text">
                                                <h5> {{str_limit($Produits->description, $limit = 45, $end = '...') }}</h5>
                                                 @if($Produits->remise == null)
                                                <h4>{{$Produits->prix}}</h4>
                                                 @else
                                                    <?php  $x =$Produits->prix * (1-$Produits->remise/100) ?>
                                                
                                                    <h4><del>{{$Produits->prix }}$</del><br> avec remise {{$Produits->remise}}% = {{$x }}$</h4>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Featured Product Area =================-->
   

 
 

 
@endsection
@extends('profil\welcome')

@section('home')   
        <!-- DataTables Example -->
         
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
           Liste de tous les livres</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Slide</th>
                    <th>Nom</th>
                    <th>Description	</th>
                    <th>remise</th>
                    <th>prix</th>
                    <th>Categorie</th> 
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                     <th>Slide</th>
                    <th>Nom</th>
                    <th>Description	</th>
                    <th>remise</th>
                    <th>prix</th>
                    <th>Categorie</th> 
                  </tr>
                </tfoot>
                <tbody>
                
                   
                  
                 @foreach( $Produitimage2 as $produits   )
                  <tr>
                    <td> 
                        <div class="l_p_img">
                            <img class="img-fluid" src={{ Voyager::image($produits->image) }} weight="70px !important" height="70px !important" alt="">
                        </div></td>
                    <td>{{$produits->nom}}</td>
                    <td>{{$produits->description}}</td>
                    <td>{{$produits->remise}}</td>
                    <td>{{$produits->prix}}</td>
                    <td>{{$produits->nomcategorie}}</td>
                    
                  </tr>

                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted"></div>
        </div>

      </div> 
@endsection
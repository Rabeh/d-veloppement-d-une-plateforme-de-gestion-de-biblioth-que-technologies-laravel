@extends('profil\welcome')

@section('home')   
        <!-- DataTables Example -->
        <div style="padding:20px">
    <form action="/Edit/{{$produits->id}}" method="POST">
        @csrf 
    <div class="form-group">
            <label for="exampleInputnom">nom</label>
            <input type="text" class="form-control" value={{$produits->nom}} name="nom" id="exampleInputnom" aria-describedby="nom Help"  >
    </div>
    <div class="form-group">
            <label for="exampleInputnom">description</label>
            <textarea class="form-control" name="description"   id="exampleFormControlTextarea1" rows="3"  > {{$produits->description}}</textarea>
    </div>
    <div class="form-group">
            <label for="exampleInputnom">prix</label>
            <input type="number" min="0" name="prix" value={{$produits->prix}} class="form-control" id="exampleInputnom" aria-describedby="nom Help" >
    </div> 
    <label for="exampleInputnom">remise</label>
    <div class="input-group"> 
            <input type="number" name="remise" class="form-control" value={{$produits->remise}}   aria-label="Amount (to the nearest dollar)">
            <div class="input-group-append">
                    <span class="input-group-text">$</span> 
            </div>
    </div><br>
    <label for="exampleInputcategorie">Categorie</label> Laste : {{$produits->nomcategorie}}
    <select class="form-control" name="Categorie"> 
            @foreach( $Categorieproduit as $Categorie   )
                    <option value={{$Categorie->id}}>{{$Categorie->nom}}</option>
            @endforeach
    </select> 
</div>
    <center>
    <button type="submit" class="btn btn-secondary" style=" padding-top: 10px;padding-bottom: 10px; padding-right: 40px; padding-left: 40px; margin-bottom: 20px;" >
                Modification
    </button> 
   </center>             
    </form>
</div>
@endsection
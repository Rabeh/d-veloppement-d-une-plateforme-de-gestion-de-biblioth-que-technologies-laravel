@extends('profil\welcome')

@section('home')   
        <!-- Profile -->
<h1>Ajoute Livre</h1>
<div style="padding:20px">
        <form action="/AjouteProduit" method="POST">
                @csrf
        <div class="form-group">
                <label for="exampleInputnom">nom</label>
                <input type="text" class="form-control" name="nom" id="exampleInputnom" aria-describedby="nom Help" placeholder="Enter Nom">
        </div>
        <div class="form-group">
                <label for="exampleInputnom">description</label>
                <textarea class="form-control" name="description"  id="exampleFormControlTextarea1" rows="3" placeholder="Enter Description"></textarea>
        </div>
        <div class="form-group">
                <label for="exampleInputnom">prix</label>
                <input type="number" min="0" name="prix" class="form-control" id="exampleInputnom" aria-describedby="nom Help" placeholder="Enter Prix">
        </div> 
        <label for="exampleInputnom">remise</label>
        <div class="input-group"> 
                <input type="number" name="remise" class="form-control" placeholder="Enter Remise" aria-label="Amount (to the nearest dollar)">
                <div class="input-group-append">
                        <span class="input-group-text">$</span> 
                </div>
        </div><br>
        <label for="exampleInputcategorie">Categorie</label>
        <select class="form-control" name="Categorie"> 
                @foreach( $Categorieproduit as $Categorie   )
                        <option value={{$Categorie->id}}>{{$Categorie->nom}}</option>
                @endforeach
        </select><br>
        <center> <button type="submit" class="btn btn-primary">Submit</button></center>
        </form>
</div>
@endsection
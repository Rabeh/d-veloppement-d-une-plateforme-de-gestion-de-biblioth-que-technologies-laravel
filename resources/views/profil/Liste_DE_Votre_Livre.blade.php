@extends('profil\welcome')

@section('home')   
        <!-- DataTables Example -->
         
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
           Liste de votre livres</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Slide</th>
                    <th>Nom</th>
                    <th>Description	</th>
                    <th>remise</th>
                    <th>prix</th>
                    <th>Categorie</th>
                    <th></th> 
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                     <th>Slide</th>
                    <th>Nom</th>
                    <th>Description	</th>
                    <th>remise</th>
                    <th>prix</th>
                    <th>Categorie</th> 
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                
                   
                  
                 @foreach( $VotreProduit as $produits   )
                  <tr>
                    <td> 
                        <div class="l_p_img">
                          @foreach( $produitimages as $produitimage   )
                            @if ($produitimage->id_produit === $produits->id )
                              <img class="img-fluid" src={{ Voyager::image($produitimage->image) }} weight="70px !important" height="70px !important" alt="">
                            @endif
                          @endforeach
                          </div></td>
                    <td>{{$produits->nom}}</td>
                    <td>{{$produits->description}}</td>
                    <td>{{$produits->remise}}</td>
                    <td>{{$produits->prix}}</td>
                    <td>{{$produits->nomcategorie}}</td>
                    <td>
                      
                        
                      <!-- Button trigger modal -->
                      <a href="/Modification/{{$produits->id}}">
                      <button type="button" class="btn btn-secondary" >
                        Modification
                      </button>
                      </a>

                      <!-- Modal -->
                      

                      <form action="{{'destroy/'.$produits->id}}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger" style="padding-left:25px;" style="padding-right:30px;">Supprimer </button>
                      </form>
                  </td>
                  </tr>

                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted"></div>
        </div>

      </div> 
@endsection